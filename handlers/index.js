var server = require('./../server/index');
var boom = require('boom');
var Joi = require('joi');
var path = require('path');
var sqlite3 = require('sqlite3');
var file = 'attractions.db';
var db = new sqlite3.Database(file);

module.exports = {
  home: function(request, reply) {
    reply.view('home', { images: request.pre.instaFetch });
  },
  findAll: function(request, reply) {
    server.methods.index.call(this, request, function(err, row) {
      if (err) return reply(err);
      reply(row);
    });
  }
};
