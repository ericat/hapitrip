var Joi = require('joi');

var attraction = {
  id: Joi.string().guid().required(),
  name: Joi.string().required(),
  type: Joi.string().required(),
};

module.exports = attraction;
