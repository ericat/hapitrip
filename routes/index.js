var Joi =  require('joi');
var handlers =  require('./../handlers');
var server = require('../server');

module.exports = [{
  method: 'GET',
  path: '/api/attractions',
  handler: handlers.findAll
}, {
  method: 'GET',
  path: '/',
  handler: handlers.home,
  config: {
    pre: [['instaFetch']]
  }
}];
