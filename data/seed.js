var file = 'attractions.db';
var attractions = require('./seed/attractions');
var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(file);

module.exports = function(cb) {
  db.serialize(function() {
    db.run("DROP TABLE IF EXISTS users");
    db.run("CREATE TABLE users (username TEXT NOT NULL, password TEXT NOT NULL, token TEXT NOT NULL)");
    db.run("INSERT INTO users VALUES ('Ericat', '$2a$10$R4cP8DMzU1ahZz8Rcv8bZ.yrSKD9ZMwmRDiE0rcMqKeDn7cPYTvDS', 'q8lrh5rzkrzdi4un8kfza5y3k1nn184x')");

    db.run("DROP TABLE IF EXISTS attractions");
    db.run("CREATE TABLE attractions (id TEXT NOT NULL PRIMARY KEY, name TEXT NOT NULL, type TEXT NOT NULL)");

    var stmt = db.prepare("INSERT INTO attractions VALUES ($1, $2, $3)");

    attractions.forEach(function(attraction, i) {
      stmt.run(attraction.id, attraction.name, attraction.type);
    });

    stmt.finalize();

    db.each("SELECT * FROM users", function(err, row) {
      console.log(row);
    });

    db.each("SELECT * FROM attractions", function(err, row) {
      console.log(row);
    });
  });

  db.close();
}

