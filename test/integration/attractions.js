var Lab = require('lab');
var lab = exports.lab = Lab.script();
var describe = lab.experiment;
var before = lab.before;
var after = lab.after;
var it = lab.it;
var Code = require('code');
var expect = Code.expect;
var Hoek = require('hoek');
var server = require('../../server');

describe('attractions', function() {
  before(function(done) {
    server.start(function() {
      done();
    });
  });

  after(function(done) {
    server.stop(function() {
      done();
    });
  });

  it('should list all attractions', function(done) {
    server.inject({
      method: 'GET',
      url: '/api/attractions'
    }, function(res) {
      expect(res.statusCode).to.be.equal(200);
      expect(res.headers['content-type']).to.include('application/json');
      expect(res.payload).to.include(['id', 'name', 'type']);
      done();
    });
  })
});
