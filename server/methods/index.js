var attractions = require('./attractions');
var lib = require('./lib');

module.exports = {
  attractions: attractions,
  lib: lib
}
