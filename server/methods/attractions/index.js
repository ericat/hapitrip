var server = require('./../../index');
var sqlite3 = require('sqlite3');
var file = 'attractions.db';
// var db = new sqlite3.Database(file);

var index = function(request, next) {
  var query = "SELECT * FROM attractions";
  var filter;

  if (request.query.type) {
    query += ' WHERE type = ?';
    filter = request.query.type;
  }
  console.log('index', this.db, this);
  this.db.all(query, filter, function(err, row) {
    if (err) return next(err);
    next(null, row);
  });
}

server.method('index', index, {});

module.exports = {
  index: index
}
