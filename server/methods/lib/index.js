var server = require('./../../index');
var config = require('../../../config');
var ig = require('instagram-node').instagram();

ig.use({
  client_id: config.instagram.id,
  client_secret: config.instagram.secret
});

function parseImages(results) {
  return results.map(function(item) {
    return item.images.low_resolution.url;
  });
}

var instaFetch = function(request, next) {
  ig.location_media_recent('742045033', function(err, results, remaining, limit) {
    var images = parseImages(results);
    if (err) return next(err);
    next(null, images);
  });
}

server.method('instaFetch', instaFetch, {});

module.exports = {
  instaFetch: instaFetch
}
