var config = require('./../config');
var Hapi = require('hapi');
var path = require('path');
var jade = require('jade');
var vision = require('vision');
var inert = require('inert');
var good = require('good');
var goodConsole = require('good-console');
var sqlite3 = require('sqlite3');
var hapiAuth = require('hapi-auth-bearer-token');
var yar =  require('yar');
var crypto =  require('crypto');
var file = 'attractions.db';
var db = new sqlite3.Database(file);
var log = console.log;

var server = module.exports = new Hapi.Server({ debug: { request: ['error'] }});
require('./methods');

// Basic Oauth
var validateOAuth = function(token, cb) {
  db.get('SELECT * from users WHERE token = ?', [token], function(err, result) {
    if (err) return cb(err, false);
    var user = result;

    if (typeof user == 'undefined') {
      return cb(null, false);
    }

    cb(null, true, {
      id: user.id,
      username: user.username
    });
  });
};

server.connection({ port: process.env.PORT || 3000 });

server.register([vision, inert, hapiAuth, {
  register: good,
  options: {
    reporters: [{
      reporter: goodConsole,
      events: { response: '*' }
    }]
  }
},{
  register: yar,
  options: {
    cookieOptions: {
      password: 'password',
      isSecure: false
    }
  }
}], function(err) {
  if (err) throw err;

  server.views({
    engines: {
      jade: jade
    },
    path: path.join(__dirname, './../views'),
    compileOptions: {
      pretty: true
    },
    relativeTo: __dirname
  });

  server.auth.strategy('api', 'bearer-access-token', {
    validateFunc: validateOAuth
  });

  server.bind({ db: db });

  server.route(require('./../routes'));
});
