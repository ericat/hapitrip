var server = require('./server/index');

server.start(function() {
  server.log('info', 'Server running at ' + server.info.uri);
});
